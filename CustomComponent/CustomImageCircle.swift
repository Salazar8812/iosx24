//
//  CustomImageCircle.swift
//  x24
//
//  Created by Josué :D on 05/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import Toucan

/// Custom TextField
@IBDesignable class CustomImageCircle: UIImageView {
    
    func setup() {
     
    }
    
    override func awakeFromNib() {
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        setup()
    }
    
}

extension UIImageView
{
    func imageBackRound(imageRound : UIImage , x : Double , y : Double , widthRadius : Double)
    {
        var imageRoundEdited = Toucan(image: imageRound).resize(CGSize(width: x, height: y), fitMode: Toucan.Resize.FitMode.crop).image
        imageRoundEdited = Toucan(image: imageRoundEdited!).maskWithEllipse(borderWidth: CGFloat(widthRadius), borderColor: UIColor.white).image
        image = imageRoundEdited
    }
}


