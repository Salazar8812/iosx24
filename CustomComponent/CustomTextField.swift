//
//  CustomTextField.swift
//  X24
//
//  Created by Josué :D on 28/01/18.
//  Copyright © 2018 noname. All rights reserved.
//

import UIKit

/// Custom TextField
@IBDesignable class CustomTextField: UITextField {
    
    /// Creating UIImageView as leftside of UITextField and Setting image for UIImageView
    @IBInspectable var leftSide:UIImage {
        get {
            return UIImage()
        } set {
            let left: UIImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 40.0, height: self.frame.size.height))
            left.image = newValue
            left.contentMode = .center
            
            leftViewMode = .always
            self.leftView = left
        }
    }
    

    @IBInspectable var backColor: UIColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5) {
        didSet {
            self.backgroundColor = backColor
        }
    }
    
    @IBInspectable var placeHolderColor : UIColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.75){
        didSet {
            setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
        }
    }
    
    @IBInspectable var textColorDefault : UIColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0){
        didSet {
            textColor = textColorDefault
        }
    }
    
    @IBInspectable var cornerRadiusCustom: Double = 5 {
        didSet {
            self.layer.cornerRadius = CGFloat(cornerRadiusCustom)
        }
    }
    
    func setup() {
        self.backgroundColor = backColor
        setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
        textColor = textColorDefault
        self.layer.cornerRadius = CGFloat(cornerRadiusCustom)
    }
    
    override func awakeFromNib() {
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        setup()
    }

}
