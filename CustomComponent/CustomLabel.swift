//
//  CustomLabel.swift
//  x24
//
//  Created by Josué :D on 15/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

@IBDesignable class CustomLabel: UILabel {


}

extension UILabel
{
    func drawLine( width1 : CGFloat , height1 : CGFloat,color : UIColor, label : UILabel , size : CGFloat , xSeparation : CGFloat)
    {
        let line = UIView(frame: CGRect(x: 0, y: height1 , width: width1 - xSeparation, height: size))
        line.backgroundColor = color
        label.addSubview(line)
    }
}



