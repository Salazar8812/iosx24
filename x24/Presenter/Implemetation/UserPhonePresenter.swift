//
//  UserPhonePresenter.swift
//  x24
//
//  Created by Charls Salazar on 14/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary


protocol UserPhonesDelegates: NSObjectProtocol {
    func OnSuccesUserPhones(userPhoneList: UserPhoneResponse)
}

class UserPhonePresenter : Base24Presenter{
   
    var userPhoneDelegate: UserPhonesDelegates!
    
    
    init(viewController: BaseViewController, userPhoneDelegate : UserPhonesDelegates) {
        super.init(viewController: viewController)
        self.userPhoneDelegate = userPhoneDelegate
    }
    
    func loadUserPhone(phone: String , company:String, token : String){
        let userPhoneRequest : UserPhoneRequest = UserPhoneRequest(phone: phone, company: company, token : token)
        RetrofitManager<UserPhoneResponse>.init(requestUrl: ApiDefinition.WS_USER_PHONES, delegate: self).requestGetWithAuthenticate(requestModel: BaseRequest())
    }
    
    func successUserPhones(requestUrl : String, userPhoneResponse : UserPhoneResponse){
        if (userPhoneResponse.listUserPhone.count>0){
            AlertDialog.hideOverlay()
            userPhoneDelegate.OnSuccesUserPhones(userPhoneList: userPhoneResponse)
        } else {
            onErrorLoadResponse(requestUrl: requestUrl, messageError: "")
        }
    }
    
    override func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        if requestUrl == ApiDefinition.WS_USER_PHONES {
            successUserPhones(requestUrl: requestUrl, userPhoneResponse: response as! UserPhoneResponse)
        }
    }
    
}
