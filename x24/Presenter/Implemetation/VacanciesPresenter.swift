//
//  VacanciesPresenter.swift
//  x24
//
//  Created by Josué :D on 27/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

protocol VacanciesDelegates: NSObjectProtocol {
    func OnSuccesVacancies(vacanciesList: VacanciesResponse)
}

class VacanciesPresenter: Base24Presenter {
    
    var vacanciesDelegate: VacanciesDelegates!
    
    init(viewController: BaseViewController, vacanciesDelegate : VacanciesDelegates) {
        super.init(viewController: viewController)
        self.vacanciesDelegate = vacanciesDelegate
    }
    
    
    func loadVacancies( token : String){
        let vacanciesRequest : VacanciesRequest = VacanciesRequest(token : token)
        RetrofitManager<VacanciesResponse>.init(requestUrl: ApiDefinition.WS_VACANCIES, delegate: self).requestGetWithAuthenticate(requestModel: BaseRequest())
    }
    
    func successVacancies(requestUrl : String, vacanciesResponse : VacanciesResponse){
        if (vacanciesResponse.listVacancies.count>0){
            AlertDialog.hideOverlay()
            vacanciesDelegate.OnSuccesVacancies(vacanciesList: vacanciesResponse)
        } else {
            onErrorLoadResponse(requestUrl: requestUrl, messageError: "")
        }
    }
    
    override func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        if requestUrl == ApiDefinition.WS_VACANCIES {
            successVacancies(requestUrl: requestUrl, vacanciesResponse: response as! VacanciesResponse)
        }
    }
}
