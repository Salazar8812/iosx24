//
//  BranchsOfficePresenter.swift
//  x24
//
//  Created by Charls Salazar on 21/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftBaseLibrary

protocol CloseToYouDelegates : NSObjectProtocol{
    func listBranchOffice(listBranchOffice: [BranchOffice])
}

class CloseToYouPresenter : Base24Presenter,CLLocationManagerDelegate{
    var mCloseToYouDelegate: CloseToYouDelegates!
    
    
    override func viewDidLoad() {
        requestLocation()
    }
    
    func loadBranchOffice( token : String){
        let closeToYouRequest : CloseToYouRequest = CloseToYouRequest(token: token)
        RetrofitManager<CloseToYouResponse>.init(requestUrl: ApiDefinition.WS_BRANCHS_OFFICES, delegate: self).requestget(requestModel: closeToYouRequest)
    }
    
    init(viewController: BaseViewController, mCloseToYouDelegate : CloseToYouDelegates) {
        super.init(viewController: viewController)
        self.mCloseToYouDelegate = mCloseToYouDelegate
    }
    
    func successLoadBranchsOffice(requestUrl : String, closeToYouResponse : CloseToYouResponse){
        AlertDialog.hideOverlay()
        print(closeToYouResponse.count ?? 0)
        print("pins: " + String((closeToYouResponse.results?.count) ?? 0))
        if(   (closeToYouResponse.results?.count) ?? 0 > 1){
            mCloseToYouDelegate.listBranchOffice(listBranchOffice: closeToYouResponse.results!)
        }else{
            AlertDialog.show(title: "Error", body: "No existen datos para cargar la informacion, ", view: mViewController)
        }
    }
    
    
    override func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        if requestUrl == ApiDefinition.WS_BRANCHS_OFFICES {
            successLoadBranchsOffice(requestUrl: requestUrl, closeToYouResponse: response as! CloseToYouResponse)
        }
    }
    
    
    func requestLocation(){
        let locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    }
}
