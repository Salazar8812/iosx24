//
//  LoginPresenter.swift
//  x24
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

protocol LoginDelegates: NSObjectProtocol {
    func OnSuccesLogin(response: LoginResponse)
}

class LoginPresenter: Base24Presenter {
    
    var loginDelegate: LoginDelegates!
    
    init(viewController: BaseViewController, loginDelegate : LoginDelegates) {
        super.init(viewController: viewController)
        self.loginDelegate = loginDelegate
    }
    
    func login(user : String, password : String, email:String){
        let loginRequest : LoginRequest = LoginRequest(user: user, password: password,email:email)
        RetrofitManager<LoginResponse>.init(requestUrl: ApiDefinition.WS_LOGIN, delegate: self).request(requestModel: loginRequest)
    }
    
    func successLogin(requestUrl : String, loginResponse : LoginResponse){
        print(loginResponse.first_name)
        print(loginResponse.thumbnail)
        if (loginResponse.token != "" && loginResponse.token != nil ){
            loginDelegate.OnSuccesLogin(response : loginResponse)
            AlertDialog.hideOverlay()
            //AlertDialog.show(title: "Ingreso", body: "Exito al ingresar", view: mViewController)
        } else {
            onErrorLoadResponse(requestUrl: requestUrl, messageError: "")
        }
    }
    
    
    override func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        if requestUrl == ApiDefinition.WS_LOGIN {
            successLogin(requestUrl: requestUrl, loginResponse: response as! LoginResponse)
        }
    }
}
