//
//  PointsPresenter.swift
//  x24
//
//  Created by Josué :D on 28/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

protocol PointsDelegates: NSObjectProtocol {
    func OnSuccesPoints(pointsList: PointsResponse)
}

class PointsPresenter: Base24Presenter {
    
    var pointsDelegate: PointsDelegates!
    
    init(viewController: BaseViewController, pointsDelegate : PointsDelegates) {
        super.init(viewController: viewController)
        self.pointsDelegate = pointsDelegate
    }
    
    
    func loadPoints( token : String){
        //let pointsRequest : PointsRequest = PointsRequest(token : token)
        let pointsRequest : VacanciesRequest = VacanciesRequest(token : token)
        RetrofitManager<PointsResponse>.init(requestUrl: ApiDefinition.WS_VACANCIES, delegate: self).requestGetWithAuthenticate(requestModel: BaseRequest())
    }
    
    func successPoints(requestUrl : String, pointsResponse : PointsResponse){
        print("points " + String(describing: pointsResponse.points))
        print("points " + String(describing: pointsResponse.count))
        if (pointsResponse.count != 0){
            AlertDialog.hideOverlay()
            pointsDelegate.OnSuccesPoints(pointsList: pointsResponse)
        } else {
            onErrorLoadResponse(requestUrl: requestUrl, messageError: "")
        }
    }
    
    override func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        if requestUrl == ApiDefinition.WS_VACANCIES {
            successPoints(requestUrl: requestUrl, pointsResponse: response as! PointsResponse)
        }
    }
}
