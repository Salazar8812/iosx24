//
//  Base24Presenter.swift
//  x24
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class Base24Presenter: BasePresenter, AlamofireResponseDelegate {
    
    
    override func viewDidLoad() {
    }
    
    func onRequestWs(){
        AlertDialog.showOverlay()
    }
    
    func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        
    }
    
    func onErrorLoadResponse(requestUrl : String, messageError : String){
        AlertDialog.hideOverlay()
        if messageError == "" {
            AlertDialog.show( title: "Error", body: StringDialogs.dialog_error_intern, view: mViewController)
        } else {
            AlertDialog.show(title: "Ha ocurrido un error", body: messageError, view: mViewController)
        }
    }
    
    func onErrorConnection(){
        AlertDialog.hideOverlay()
        AlertDialog.show( title: "Error", body: StringDialogs.dialog_error_connection, view: mViewController)
    }
}
