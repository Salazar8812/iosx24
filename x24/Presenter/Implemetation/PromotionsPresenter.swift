//
//  PromotionsPresenter.swift
//  x24
//
//  Created by Josué :D on 20/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

protocol PromotionsDelegates: NSObjectProtocol {
    func OnSuccesPromotions(promotionsList: PromotionsResponse)
}

class PromotionsPresenter: Base24Presenter {
    
    var promotionsDelegate: PromotionsDelegates!
    
    init(viewController: BaseViewController, promotionsDelegate : PromotionsDelegates) {
        super.init(viewController: viewController)
        self.promotionsDelegate = promotionsDelegate
    }

    
    func loadPromotions( token : String){
        let promotionsRequest : PromotionsRequest = PromotionsRequest(token : token)
        RetrofitManager<PromotionsResponse>.init(requestUrl: ApiDefinition.WS_PROMOTIONS, delegate: self).requestGetWithAuthenticate(requestModel: BaseRequest())
    }
    
    func successPromotions(requestUrl : String, promotionsResponse : PromotionsResponse){
        if (promotionsResponse.listPromotions.count>0){
            AlertDialog.hideOverlay()
            promotionsDelegate.OnSuccesPromotions(promotionsList: promotionsResponse)
        } else {
            onErrorLoadResponse(requestUrl: requestUrl, messageError: "")
        }
    }
    
    override func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        if requestUrl == ApiDefinition.WS_PROMOTIONS {
            successPromotions(requestUrl: requestUrl, promotionsResponse: response as! PromotionsResponse)
        }
    }
}
