//
//  UserPhoneRequest.swift
//  x24
//
//  Created by Charls Salazar on 14/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation
import SwiftBaseLibrary
import ObjectMapper

class UserPhoneRequest: BaseRequest{
    var phone : String?
    var company: String?
    var token: String?
    
    init(phone: String, company: String, token : String) {
        super.init()
        self.phone = phone
        self.company = company
        self.token = token
    }
    
    public required init?(map: Map) {
        super.init()
    }
    
    internal override func mapping(map: Map) {
        super.mapping(map: map)
        self.phone <- map["phone"]
        self.company <- map["company"]
        self.token <- map["token"]
    }
    
}
