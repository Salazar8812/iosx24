//
//  VacanciesRequest.swift
//  x24
//
//  Created by Josué :D on 27/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class VacanciesRequest: BaseRequest {
    var token: String?
    
    init(token: String) {
        super.init()
    }
    
    public required init?(map: Map) {
        super.init()
    }
    
    internal override func mapping(map: Map) {
        super.mapping(map: map)
        self.token <- map["token"]
    }
    
}
