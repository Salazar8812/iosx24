//
//  LoginRequest.swift
//  x24
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class LoginRequest: BaseRequest {
    var userName: String?
    var password:String?
    var email: String?
    
    init(user: String, password: String, email:String) {
        super.init()
        self.userName = user
        self.password = password
        self.email = email
    }
    
    public required init?(map: Map) {
        super.init()
    }
    
    internal override func mapping(map: Map) {
        super.mapping(map: map)
        self.userName <- map["username"]
        self.password <- map["password"]
        self.email <- map["email"]
    }
    
}
