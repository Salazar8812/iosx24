//
//  RetrofitManager.swift
//  x24
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class RetrofitManager<Res : BaseResponse>: BaseRetrofitManager<Res> {
    
    override func getDebugEnabled() -> Bool {
        return false
    }
    
    override func getJsonDebug(requestUrl : String) -> String {
        var json = ""
        switch requestUrl {
        default: break
        }
        
        return json
    }
}
