//
//  Definitions.swift
//  x24
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation

class ApiDefinition: NSObject {
    //static let API_SERVER = "http://188.166.105.203:8000/"
    static let API_SERVER = "http://ivanysusamikos.com:8000/"

    static let WS_LOGIN = API_SERVER + "accounts/api/v1/login/"
    static let WS_BRANDS = API_SERVER +  "business/api/v1/brands/"
    static let WS_PHONE_COMPANIES = API_SERVER + "business/api/v1/phone_companies/"
    static let WS_USER_PHONES = API_SERVER + "business/api/v1/user_phones/"
    static let WS_PROMOTIONS = API_SERVER + "business/api/v1/promotions/"
    static let WS_USERS = API_SERVER + "business/api/v1/users/"
    static let WS_BRANCHS_OFFICES = API_SERVER + "business/api/v1/branch_offices/"
    static let WS_VACANCIES = API_SERVER + "business/api/v1/vacants/"
    static let WS_USER_POINTS = API_SERVER + "business/api/v1/user_points/"
    
}
