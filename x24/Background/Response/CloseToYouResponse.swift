//
//  CloseToYouResponse.swift
//  x24
//
//  Created by Charls Salazar on 21/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation
import SwiftBaseLibrary
import ObjectMapper

class CloseToYouResponse : BaseResponse{
    var count: Int?
    var next : String?
    var previous : String?
    var results : [BranchOffice]?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal override func mapping(map: Map) {
        super.mapping(map: map)
        self.count <- map["count"]
        self.next <- map["next"]
        self.previous <- map["previous"]
        self.results <- map["results"]
    }
}
