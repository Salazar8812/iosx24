//
//  VacanciesResponse.swift
//  x24
//
//  Created by Josué :D on 27/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class VacanciesResponse: BaseResponse {
    var count: String?
    var next : String?
    var previous : String?
    var listVacancies : [Vacancies] = []
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal override func mapping(map: Map) {
        super.mapping(map: map)
        self.count <- map["count"]
        self.next <- map["next"]
        self.previous <- map["previous"]
        self.listVacancies <- map["results"]
    }
}
