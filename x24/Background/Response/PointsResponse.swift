//
//  PointsResponse.swift
//  x24
//
//  Created by Josué :D on 28/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class PointsResponse: BaseResponse {
    
    var points : Int?
    var count : Int?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal override func mapping(map: Map) {
        super.mapping(map: map)
        print(map["points"])
        self.points <- map["points"]
        self.count <- map["count"]
    }
}

