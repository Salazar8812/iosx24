//
//  LoginResponse.swift
//  x24
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class LoginResponse: BaseResponse {
    var token : String?
    var username : String?
    var email : String?
    var id : String?
    var thumbnail : String?
    var first_name : String?
    var last_name : String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal override func mapping(map: Map) {
        super.mapping(map: map)
        self.token <- map["token"]
        self.username <- map["username"]
        self.email <- map["email"]
        self.id <- map["id"]
        self.thumbnail <- map["thumbnail"]
        self.first_name <- map["first_name"]
        self.last_name <- map["last_name"]
    }
}
