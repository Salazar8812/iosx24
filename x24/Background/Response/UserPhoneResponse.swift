//
//  UserPhoneResponse.swift
//  x24
//
//  Created by Charls Salazar on 14/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation
import SwiftBaseLibrary
import ObjectMapper

class UserPhoneResponse : BaseResponse{
    var listUserPhone : [UserPhone] = []
    var count : String?
    var next : String?
    var previous : String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal override func mapping(map: Map) {
        super.mapping(map: map)
        self.listUserPhone <- map["results"]
        self.count <- map["count"]
        self.next <- map["next"]
        self.previous <- map["previous"]
    }
    
}

