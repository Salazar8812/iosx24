//
//  PromotionsResponse.swift
//  x24
//
//  Created by Josué :D on 20/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class PromotionsResponse: BaseResponse {
    var count: String?
    var next : String?
    var previous : String?
    var listPromotions : [Promotion] = []
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal override func mapping(map: Map) {
        super.mapping(map: map)
        self.count <- map["count"]
        self.next <- map["next"]
        self.previous <- map["previous"]
        self.listPromotions <- map["results"]
    }
}
