//
//  ViewController.swift
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController,TableViewCellClickDelegate{
    @IBOutlet weak var mListPromotionTableView: UITableView!
    var baseDataSourcePromotion : PromotionDataSource<NSObject, PromotionTableViewCell>?
    //var listPromotion : [Promotion] = []
    var promotionsPresenter : PromotionsPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
        //promotionsPresenter.loadPromotions(token: "a")
        
        //baseDataSourcePromotion = PromotionDataSource(tableView: mListPromotionTableView,delegate: self)
        //populatePromotion()
        //baseDataSourcePromotion?.update(items: listPromotion)
    }
    
//    override func getPresenter() -> BasePresenter? {
//        promotionsPresenter = PromotionsPresenter(viewController: self, promotionsDelegate: self as! PromotionsDelegates)
//        return promotionsPresenter
//    }
    
    
//    func populatePromotion(){
//        let promo1 = Promotion()
//        promo1.image = "ic_promo1";
//        listPromotion.append(promo1)
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        ViewControllerUtils.pushViewController(from: self, to: DetailPromotionViewController.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.navigationItem.title = "Promociones"
    }
}


