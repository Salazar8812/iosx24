//
//  MapViewController
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class MapViewController: BaseViewController, GMSMapViewDelegate, CloseToYouDelegates,CLLocationManagerDelegate {
    
    @IBOutlet weak var mGmapGMSMapView: GMSMapView!
    var mUserPosition : CLLocationCoordinate2D!
    var mCloseToYouPresenter : CloseToYouPresenter!
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mGmapGMSMapView.delegate = self
        mGmapGMSMapView.isMyLocationEnabled = true
        
        addPullUpController()
        //mCloseToYouPresenter.loadBranchOffice(token: "398e92b9bf4903abd27f3c6d0bbca82d5b26b64e")
        
        let tokenUser = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_TOKEN)!)
        print("TERMINAL_TOKEN " + tokenUser )
        mCloseToYouPresenter.loadBranchOffice(token: tokenUser)
        
        let camera = GMSCameraPosition.camera(withLatitude: (19.3843749), longitude: (-99.1778559), zoom: 11.0)
        mGmapGMSMapView.animate(to: camera)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getLocation()
        self.setNavigationBarItem()
    }

    private func addPullUpController() {
        guard
            let pullUpController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "SearchStoreViewController") as? SearchStoreViewController
            else { return }
        
        addPullUpController(pullUpController)
    }
    

    override func getPresenter() -> BasePresenter? {
        mCloseToYouPresenter = CloseToYouPresenter(viewController: self, mCloseToYouDelegate : self as CloseToYouDelegates)
        return mCloseToYouPresenter
    }
    
//    override func getPresenter() -> BasePresenter? {
//        vacanciesPresenter = VacanciesPresenter(viewController: self, vacanciesDelegate: self as VacanciesDelegates)
//        return vacanciesPresenter
//    }
    
    func addMarkerInMap(lat: Double , long: Double, title: String, snippet : String){
        let marker = GMSMarker()
        mUserPosition = CLLocationCoordinate2D(latitude: (lat), longitude: (long))
        marker.position = mUserPosition
        marker.title = title
        marker.snippet = snippet
        marker.icon = UIImage(named: "ic_marker")
        marker.map = mGmapGMSMapView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let customInfoWindow = Bundle.main.loadNibNamed("MapInfoWindowRView", owner: self, options: nil)![0] as! MapInfoWindowRView
        customInfoWindow.mTitleLabel.text = marker.title
        customInfoWindow.mAddressLabel.text = marker.snippet
        
        return customInfoWindow
    }
    
    func listBranchOffice(listBranchOffice: [BranchOffice]) {
        for item in listBranchOffice{
            addMarkerInMap(lat: item.latitude!, long: item.longitude!, title: item.name! , snippet: item.full_address!)
        }
    }
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print(marker.title!)
        print(marker.snippet!)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func getLocation(){
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()
    }
}
