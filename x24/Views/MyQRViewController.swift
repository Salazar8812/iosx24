//
//  MyQRViewController.swift
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import ISPageControl
import SwiftBaseLibrary

class MyQRViewController: BaseViewController,NumberPhoneDataDelegate, UserPhonesDelegates{
    
    var numberPhoneDataSource : NumberPhoneDataSource?
    @IBOutlet weak var mIsPageControl: ISPageControl!
    @IBOutlet weak var mListPhoneNumberiCarousel: iCarousel!
    var userPhonesPresenter: UserPhonePresenter?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userPhonesPresenter?.loadUserPhone(phone: "", company: "", token : "398e92b9bf4903abd27f3c6d0bbca82d5b26b64e")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    override func getPresenter() -> BasePresenter? {
        userPhonesPresenter = UserPhonePresenter(viewController: self, userPhoneDelegate: self)
        return userPhonesPresenter
    }
    
    @IBAction func mAddNumberButton(_ sender: Any) {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomDialogViewController") as! CustomDialogViewController
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.mCustomDialogDelegate = self
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func onItemClick(numberSelected: UserPhone) {
        let actionSheetController = UIAlertController(title: numberSelected.user, message: numberSelected.phone, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancelar", style: .cancel) { action -> Void in

        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Editar", style: .default) { action -> Void in

        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Borrar", style: .default) { action -> Void in

        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func OnSuccesUserPhones(userPhoneList: UserPhoneResponse) {
        numberPhoneDataSource = NumberPhoneDataSource(carrusel: mListPhoneNumberiCarousel, mNumberPhoneDataDelegate: self, mPageControl: mIsPageControl)
        numberPhoneDataSource?.update(items: userPhoneList.listUserPhone)
        numberPhoneDataSource?.settings()
        mIsPageControl.borderColor = UIColor(netHex: Colors.color_red)
        mIsPageControl.currentPageTintColor = UIColor(netHex: Colors.color_red)
    }
    
}

extension BaseViewController: CustomDialogDelegate {
    func OnClickCancel() {
        
    }
    
    func OnClickOk() {
    
    }
    
    
    
}
