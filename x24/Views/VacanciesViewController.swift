//
//  VacanciesViewController.swift
//  x24
//
//  Created by Josué :D on 18/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class VacanciesViewController: BaseViewController, TableViewCellClickDelegate, VacanciesDelegates {

    @IBOutlet var tableView: UITableView!
    
    var baseDataSourceVacancies : VacanciesDataSource<NSObject, VacanciesTableViewCell>?
    var vacanciesPresenter : VacanciesPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        baseDataSourceVacancies = VacanciesDataSource(tableView: tableView,delegate: self)
        vacanciesPresenter.loadVacancies(token: "398e92b9bf4903abd27f3c6d0bbca82d5b26b64e")
        baseDataSourceVacancies = VacanciesDataSource(tableView: tableView,delegate: self)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func getPresenter() -> BasePresenter? {
        vacanciesPresenter = VacanciesPresenter(viewController: self, vacanciesDelegate: self as VacanciesDelegates)
        return vacanciesPresenter
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.navigationItem.title = "Vacantes"
    }
    
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        //ViewControllerUtils.pushViewController(from: self, to: DetailPromotionViewController.self)
    }
    
    func OnSuccesVacancies(vacanciesList: VacanciesResponse) {
         baseDataSourceVacancies?.update(items: vacanciesList.listVacancies)
    }
    
    

    /*
    // MARK: - Navigation

    // In a 2-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
