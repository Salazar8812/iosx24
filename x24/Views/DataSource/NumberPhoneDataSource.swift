//
//  NumberPhoneDataSource.swift
//  x24
//
//  Created by Charls Salazar on 31/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation
import ISPageControl

protocol NumberPhoneDataDelegate: NSObjectProtocol {
    func onItemClick(numberSelected: UserPhone)
}

class NumberPhoneDataSource: NSObject, iCarouselDataSource, iCarouselDelegate {
    
    var mPageControl : ISPageControl?
    var mICarousel: iCarousel
    var mItems : [UserPhone] = []
    var mSelectedIndex : Int = 0
    var mNumberPhoneDataDelegate : NumberPhoneDataDelegate?

    
    
    init(carrusel : iCarousel, mNumberPhoneDataDelegate: NumberPhoneDataDelegate, mPageControl : ISPageControl){
        mICarousel = carrusel
        self.mNumberPhoneDataDelegate = mNumberPhoneDataDelegate
        self.mPageControl = mPageControl
    }
    
    func settings(){
        mICarousel.type = iCarouselType.coverFlow2
        mICarousel.delegate = self
        mICarousel.dataSource = self
    }
    
    func update(items : [UserPhone]){
        mItems = items
        mICarousel.reloadData()
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        mPageControl?.numberOfPages = mItems.count
        return mItems.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let view = Bundle.main.loadNibNamed("ItemNumberCollectionReusableView", owner: self, options: nil)![0] as! ItemNumberCollectionReusableView
        view.mPhoneLabel.text = mItems[index].phone
        view.mTitleParent.text = mItems[index].user
        
        let imageQrEdited = UIImage(named : "ic_qr")
        var imageQr = processPixelsInImage(imageQrEdited!)
        view.mCodeQrImageView.image = imageQr
  
        //bimageQrEdited?.getPixelColor(point: CGPoint(x: 1.0, y: 1.0))
       
        
        return view
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        mSelectedIndex = index
        mNumberPhoneDataDelegate?.onItemClick(numberSelected: mItems[mSelectedIndex])
    }
    
    func carouselDidScroll(_ carousel: iCarousel) {
        mPageControl?.currentPage = Int(carousel.currentItemIndex)
    }
    
    
    
    
    
    
    
    func getPixelColor(_ image:UIImage, _ point: CGPoint) -> UIColor {
        let cgImage : CGImage = image.cgImage!
        guard let pixelData = CGDataProvider(data: (cgImage.dataProvider?.data)!)?.data else {
            return UIColor.clear
        }
        let data = CFDataGetBytePtr(pixelData)!
        let x = Int(point.x)
        let y = Int(point.y)
        let index = Int(image.size.width) * y + x
        let expectedLengthA = Int(image.size.width * image.size.height)
        let expectedLengthRGB = 3 * expectedLengthA
        let expectedLengthRGBA = 4 * expectedLengthA
        let numBytes = CFDataGetLength(pixelData)
        switch numBytes {
        case expectedLengthA:
            return UIColor(red: 0, green: 0, blue: 0, alpha: CGFloat(data[index])/255.0)
        case expectedLengthRGB:
            return UIColor(red: CGFloat(data[3*index])/255.0, green: CGFloat(data[3*index+1])/255.0, blue: CGFloat(data[3*index+2])/255.0, alpha: 1.0)
        case expectedLengthRGBA:
            return UIColor(red: CGFloat(data[4*index])/255.0, green: CGFloat(data[4*index+1])/255.0, blue: CGFloat(data[4*index+2])/255.0, alpha: CGFloat(data[4*index+3])/255.0)
        default:
            // unsupported format
            return UIColor.clear
        }
    }
    
    
    func processPixelsInImage(_ image: UIImage) -> UIImage? {
        guard let inputCGImage = image.cgImage else {
            //print("unable to get cgImage")
            return nil
        }
        let colorSpace       = CGColorSpaceCreateDeviceRGB()
        let width            = inputCGImage.width
        let height           = inputCGImage.height
        let bytesPerPixel    = 4
        let bitsPerComponent = 8
        let bytesPerRow      = bytesPerPixel * width
        let bitmapInfo       = RGBA32.bitmapInfo
        
        guard let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo) else {
            //print("unable to create context")
            return nil
        }
        context.draw(inputCGImage, in: CGRect(x: 0, y: 0, width: width, height: height))
        
        guard let buffer = context.data else {
            //print("unable to get context data")
            return nil
        }
        
        let pixelBuffer = buffer.bindMemory(to: RGBA32.self, capacity: width * height)
        
        let white = RGBA32(red: 255, green: 255, blue: 255, alpha: 255)
        let red = RGBA32(red: 255, green: 1, blue: 1, alpha: 255)
        let clear = RGBA32(red: 0, green: 0, blue: 0, alpha: 0)
        
        for row in 0 ..< Int(height) {
            for column in 0 ..< Int(width) {
                let offset = row * width + column
                if pixelBuffer[offset] == white {
                    pixelBuffer[offset] = clear
                }
                else
                {
                    
                    let numberNormalized = Double(255.0 / Double(height))
                    //print(numberNormalized)
                    //print(Int(height))
                    var numberColorNormalized = numberNormalized * Double(row)
                    //print("color ")
                    //print(numberColorNormalized)
                    let newColor = RGBA32(red: UInt8(numberColorNormalized), green: 1, blue: 1, alpha: 255)
                    
                    pixelBuffer[offset] = newColor
                }
                
            }
        }
        
        let outputCGImage = context.makeImage()!
        let outputImage = UIImage(cgImage: outputCGImage, scale: image.scale, orientation: image.imageOrientation)
        
        return outputImage
        
    }
    
    
    struct RGBA32: Equatable {
        var color: UInt32
        
        var red: UInt8 {
            return UInt8((color >> 24) & 255)
        }
        
        var green: UInt8 {
            return UInt8((color >> 16) & 255)
        }
        
        var blue: UInt8 {
            return UInt8((color >> 8) & 255)
        }
        
        var alpha: UInt8 {
            return UInt8((color >> 0) & 255)
        }
        
        init(red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8) {
            //color = (UInt32(red) << 24) | (UInt32(green) << 16) | (UInt32(blue) << 8) | (UInt32(alpha) << 0)
            var color1 = (UInt32(red) << 24) | (UInt32(green) << 16)
            var color2 = (UInt32(blue) << 8) | (UInt32(alpha) << 0)
            color = color1 | color2
            //color = (UInt32(red)) | (UInt32(green) ) | (UInt32(blue)) | (UInt32(alpha) << 0)
        }
        
        static let bitmapInfo = CGImageAlphaInfo.premultipliedLast.rawValue | CGBitmapInfo.byteOrder32Little.rawValue
        
        static func ==(lhs: RGBA32, rhs: RGBA32) -> Bool {
            return lhs.color == rhs.color
        }
        
    }
    
    
    
}
