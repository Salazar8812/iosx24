//
//  BaseDatoSource.swift
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class BaseDataSource<T: NSObject, C :BaseTableViewCell>: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var items : [T] = []
    var tableView : UITableView?
    var identifier : String?
    var executeAction : Bool = false
    var delegate : TableViewCellClickDelegate?
    
    init(tableView: UITableView) {
        super.init()
        self.tableView = tableView
        self.identifier = "\(self.genericName())"
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.delegate = nil
    }
    
    init(tableView: UITableView, delegate : TableViewCellClickDelegate) {
        super.init()
        self.tableView = tableView
        self.identifier = "\(self.genericName())"
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.delegate = delegate
    }
    
    func update(items: [T]) {
        self.items = items
        self.tableView?.reloadData()
        self.executeAction = false
    }
    
    func update(items: [T], action : Bool) {
        self.items = items
        self.tableView?.reloadData()
        self.executeAction = action
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
//    func tableView(_tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
//        return 100
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        if indexPath.row == 0
        {
            let maincell = tableView.dequeueReusableCell(withIdentifier: "MainMenuTableViewCell") as! MainMenuTableViewCell
            
            let item = items[indexPath.row]
            maincell.pupulate(object: item)
            
            if executeAction {
                maincell.executeAction()
            }
            
            if delegate != nil{
                maincell.delegate = delegate
            }
            
            return maincell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier!) as! BaseTableViewCell
            
            let item = items[indexPath.row]
            cell.pupulate(object: item)
            
            if executeAction {
                cell.executeAction()
            }
            
            if delegate != nil{
                cell.delegate = delegate
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
            print("TERMINAL_LOGIN " + result )
            
            if (result) == "true"
            {
                let item = items[indexPath.row] as? Menu
                item?.tileMenu = "Mi Perfil"
                tableView.deselectRow(at: indexPath, animated: true)
                let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!
                self.delegate?.onTableViewCellClick(item: item as! Menu, cell: cell)
            }
            else
            {
                let item = items[indexPath.row] as? Menu
                item?.tileMenu = "CeldaPrimerDato"
                tableView.deselectRow(at: indexPath, animated: true)
                let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!
                self.delegate?.onTableViewCellClick(item: item as! Menu, cell: cell)
            }
        }
        else
        {
            let item = items[indexPath.row]
            tableView.deselectRow(at: indexPath, animated: true)
            let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!
            self.delegate?.onTableViewCellClick(item: item as! Menu, cell: cell)
        }
        
    }
    
    func genericName() -> String {
        let fullName: String = NSStringFromClass(C.self)
        let range = fullName.range(of: ".", options: .backwards, range: nil, locale: nil)
        if let range = range {
            return fullName.substring(from: range.upperBound)
        } else {
            return fullName
        }
    }
    
}


