//
//  VacanciesDataSource.swift
//  x24
//
//  Created by Josué :D on 18/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation

class VacanciesDataSource<T: NSObject, C :BaseTableViewCell>: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var items : [T] = []
    var tableView : UITableView?
    var identifier : String?
    var executeAction : Bool = false
    var delegate : TableViewCellClickDelegate?
    
    init(tableView: UITableView) {
        super.init()
        self.tableView = tableView
        self.identifier = "\(self.genericName())"
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.delegate = nil
    }
    
    init(tableView: UITableView, delegate : TableViewCellClickDelegate) {
        super.init()
        self.tableView = tableView
        self.identifier = "\(self.genericName())"
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.delegate = delegate
    }
    
    func update(items: [T]) {
        self.items = items
        self.tableView?.reloadData()
        self.executeAction = false
    }
    
    func update(items: [T], action : Bool) {
        self.items = items
        self.tableView?.reloadData()
        self.executeAction = action
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.items.count)
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier!) as! BaseTableViewCell
        
        let item = items[indexPath.row]
        cell.pupulate(object: item)
        
        if executeAction {
            cell.executeAction()
        }
        
        if delegate != nil{
            cell.delegate = delegate
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!
        self.delegate?.onTableViewCellClick(item: item, cell: cell)
    }
    
    func genericName() -> String {
        let fullName: String = NSStringFromClass(C.self)
        let range = fullName.range(of: ".", options: .backwards, range: nil, locale: nil)
        if let range = range {
            return fullName.substring(from: range.upperBound)
        } else {
            return fullName
        }
    }
    
}


