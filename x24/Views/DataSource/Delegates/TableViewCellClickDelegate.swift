//
//  TableViewClickDelegate.swift
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

public protocol TableViewCellClickDelegate: NSObjectProtocol {
    
    func onTableViewCellClick(item: NSObject, cell : UITableViewCell)
    
}

