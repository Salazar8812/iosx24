//
//  PromotionsViewController.swift
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class PromotionsViewController: BaseViewController, TableViewCellClickDelegate , PromotionsDelegates{
    
    @IBOutlet weak var mListPromotionTableView: UITableView!
    var baseDataSourcePromotion : PromotionDataSource<NSObject, PromotionTableViewCell>?
    var promotionsPresenter : PromotionsPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        promotionsPresenter.loadPromotions(token: "398e92b9bf4903abd27f3c6d0bbca82d5b26b64e")
        baseDataSourcePromotion = PromotionDataSource(tableView: mListPromotionTableView,delegate: self)
        
    }

    override func getPresenter() -> BasePresenter? {
        promotionsPresenter = PromotionsPresenter(viewController: self, promotionsDelegate: self as PromotionsDelegates)
        return promotionsPresenter
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
          self.navigationItem.title = "Promociones"
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        ViewControllerUtils.pushViewController(from: self, to: DetailPromotionViewController.self)
    }
    
    func OnSuccesPromotions(promotionsList: PromotionsResponse) {
        baseDataSourcePromotion?.update(items: promotionsList.listPromotions)
    }

}
