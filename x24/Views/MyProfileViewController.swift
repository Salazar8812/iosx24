//
//  MyProfileViewController.swift
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class MyProfileViewController: BaseViewController,PointsDelegates {

    
    
    @IBOutlet var imageTopBack: UIImageView!
    @IBOutlet var cellphone: UILabel!
    @IBOutlet var user: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var buttonChangePassword: UIButton!
    @IBOutlet var imageProfile: UIImageView!
    @IBOutlet var points: UILabel!
    
    @IBOutlet var imageBack: UIImageView!
    
    var pointsPresenter : PointsPresenter!
    
    override func viewWillAppear(_ animated: Bool) {
        let transitionOptions = UIViewAnimationOptions.curveEaseIn
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            self.setNavigationBarItem()
        }, completion: { finished in
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let token = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_TOKEN)!)
        //pointsPresenter.loadPoints(token: token)
        pointsPresenter.loadPoints(token: "398e92b9bf4903abd27f3c6d0bbca82d5b26b64e")
        
        
         let image = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_THUMBNAIL)!)
         let emailUser = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_EMAIL)!)
         let nameUser = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_USERNAME)!)
        
        imageProfile.setRounded()
        imageProfile.image =  UIImage(named : "ic_account")
        imageProfile.moa.url = ApiDefinition.API_SERVER + (image)
        imageProfile.moa.errorImage = UIImage(named: "ic_account")
        
        //imageProfile.imageBackRound(imageRound: imageProfileTemp!, x: Double(imageProfile.frame.height + 50), y: Double(imageProfile.frame.height + 50), widthRadius: 2)
        
        user.text = emailUser
        email.text = nameUser
        points.text = "0"
        
        cellphone.drawLine( width1: cellphone.frame.size.width, height1: cellphone.frame.size.height, color: UIColor.red, label: cellphone, size : 0.5, xSeparation : 40)
        user.drawLine( width1: cellphone.frame.size.width, height1: cellphone.frame.size.height, color: UIColor.red, label: user , size : 0.5, xSeparation : 40)
        email.drawLine( width1: cellphone.frame.size.width, height1: cellphone.frame.size.height, color: UIColor.red, label: email, size : 0.5, xSeparation : 40)
        
    }
    
    override func getPresenter() -> BasePresenter? {
        pointsPresenter = PointsPresenter(viewController: self, pointsDelegate: self as PointsDelegates)
        return pointsPresenter
    }
    
    func OnSuccesPoints(pointsList: PointsResponse) {
        points.text = String(describing: pointsList.count!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
