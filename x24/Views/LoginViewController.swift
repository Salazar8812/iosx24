//
//  LoginViewController.swift
//  x24
//
//  Created by Josué :D on 30/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController, LoginDelegates {
    
    @IBOutlet var ingresar: UIButton!
    @IBOutlet var correo: UITextField!
    @IBOutlet var contraseña: UITextField!
    @IBOutlet var scrollView: UIScrollView!

    @IBOutlet var btnIngresar: UIButton!
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var btnFb: UIButton!
    @IBOutlet var btnGoogle: UIButton!
    
    var loginPresenter : LoginPresenter!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.navigationItem.title = "Ingresar"
        scrollView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    override func getPresenter() -> BasePresenter? {
        loginPresenter = LoginPresenter(viewController: self, loginDelegate: self as LoginDelegates)
        return loginPresenter
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        loginPresenter.login(user: correo.text!, password: contraseña.text!, email:"" )
    }
    
    @IBAction func actionCreateAccount(_ sender: Any) {
        let pushController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    
    @IBAction func actionForgotPassword(_ sender: Any) {
        let pushController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    func OnSuccesLogin(response: LoginResponse) {
        UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
        UserDefaults.standard.setValue(response.token, forKey: DataPersistent.TERMINAL_TOKEN)
        UserDefaults.standard.setValue(response.username, forKey: DataPersistent.TERMINAL_USERNAME)
        UserDefaults.standard.setValue(response.email, forKey: DataPersistent.TERMINAL_EMAIL)
        UserDefaults.standard.setValue(response.id, forKey: DataPersistent.TERMINAL_ID)
        UserDefaults.standard.setValue(response.thumbnail, forKey: DataPersistent.TERMINAL_THUMBNAIL)
        UserDefaults.standard.setValue(response.first_name, forKey: DataPersistent.TERMINAL_FIRST_NAME)
        UserDefaults.standard.setValue(response.last_name, forKey: DataPersistent.TERMINAL_LAST_NAME)
        
        UserDefaults.standard.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        let mainViewController = UINavigationController(rootViewController: mainController)
        self.slideMenuController()?.changeMainViewController(mainViewController, close: true)
        
        
        //let menuController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        //menuController.defineMenu()
        
        leftViewController.defineMenu()
    }
    
}

