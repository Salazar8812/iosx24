//
//  ItemNumberCollectionReusableView.swift
//  x24
//
//  Created by Charls Salazar on 31/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class ItemNumberCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var mCodeQrImageView: UIImageView!
    @IBOutlet weak var mPhoneLabel: UILabel!
    @IBOutlet weak var mTitleParent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
