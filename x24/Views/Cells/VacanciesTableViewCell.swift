//
//  VacanciesTableViewCell.swift
//  x24
//
//  Created by Josué :D on 18/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class VacanciesTableViewCell: BaseTableViewCell {

    var item : Vacancies?
    
    @IBOutlet var labelAdress: UILabel!
    @IBOutlet var labelVacancyNumber: UILabel!
    @IBOutlet var labelTurn: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func pupulate(object :NSObject) {
        item = (object as! Vacancies)
        labelAdress.text = item?.description2
        //labelVacancyNumber.text = "Vacante: " + item!.vacancyNumber!
        //labelTurn.text = "Turno: " + item!.turn!
    }
    
    override func toString() -> String{
        return "VacanciesTableViewCell"
    }

}
