//
//  PromotionTableViewCell.swift
//  x24
//
//  Created by Charls Salazar on 31/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import moa

class PromotionTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var mPromotionImageView: UIImageView!

    @IBOutlet var labelTitle: UILabel!
    
    @IBOutlet var labelDescription: UILabel!
    
    @IBOutlet var lblDescription: UILabel!
    
    @IBOutlet var lblTitle: UILabel!
    
    
    var item : Promotion?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func pupulate(object :NSObject) {
        item = object as? Promotion
        
        mPromotionImageView.moa.url = ApiDefinition.API_SERVER + (item?.image)!
        mPromotionImageView.moa.errorImage = UIImage(named: "logoX24.png")

        lblTitle.text = item?.title
        lblDescription.text = item?.description2
    }
    
    override func toString() -> String{
        return "PromotionTableViewCell"
    }

}
