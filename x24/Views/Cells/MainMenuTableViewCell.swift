//
//  MainMenuTableViewCell.swift
//  x24
//
//  Created by Josué :D on 30/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class MainMenuTableViewCell: BaseTableViewCell {
    
    
    
    @IBOutlet var backImage: UIImageView!
    

    @IBOutlet weak var imageProfile: UIImageView!
    
    @IBOutlet var welcome: UILabel!
    
    @IBOutlet var name: UILabel!
    
    var item : Menu?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageProfile.setRounded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    

    override func pupulate(object :NSObject) {
        
       let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
        
        if result == "true"
        {
            let nameUser = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_USERNAME)!)
            let email = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_EMAIL)!)
            let image = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_THUMBNAIL)!)
            
            imageProfile.image =  UIImage(named : "ic_account")
            imageProfile.moa.url = ApiDefinition.API_SERVER + (image)
            imageProfile.moa.errorImage = UIImage(named: "ic_account")
            
            backImage.image = UIImage(named : "fondox24.jpeg")
            name.text = email
            welcome.text = nameUser
        }
        else
        {
            imageProfile.image =  UIImage(named : "ic_account")
            backImage.image = UIImage(named : "ic_background")
            name.text = "Para iniciar sesiòn, haga click aquí"
            name.adjustsFontSizeToFitWidth = true
            welcome.text = ""
        }
        

    }
    
    override func toString() -> String{
        return "MainMenuTableViewCell"
    }
    
}


