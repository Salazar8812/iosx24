//
//  BaseTableViewCell.swift
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    var delegate : NSObjectProtocol!
    var itemObject : NSObject?
    
    func pupulate(object :NSObject) {
        preconditionFailure("This method must be overridden")
    }
    
    func executeAction() {
        preconditionFailure("This method must be overridden")
    }
    
    func toString() -> String{
        preconditionFailure("This method must be overridden")
    }
    
}
