//
//  OptionMenuTableViewCell.swift
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class OptionMenuTableViewCell: BaseTableViewCell {

    @IBOutlet weak var mTitleMenuLabel: UILabel!
    @IBOutlet weak var mIconImageImageView: UIImageView!
    var item : Menu?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func pupulate(object :NSObject) {
        item = object as? Menu
        mTitleMenuLabel.text = item?.tileMenu!
        mIconImageImageView.image = UIImage(named: (item?.iconImageMenu)!)
    }
    
    
    override func toString() -> String{
        return "OptionMenuTableViewCell"
    }

}
