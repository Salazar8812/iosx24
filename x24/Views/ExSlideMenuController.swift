//
//  ExSlideMenuController.swift
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class ExSlideMenuController : SlideMenuController {
    
    override func isTagetViewController() -> Bool {
        if let vc = UIApplication.topViewController() {
            if vc is MainViewController ||
                vc is LoginViewController ||
                vc is PromotionsViewController ||
                vc is MapViewController ||
                vc is MyQRViewController ||
                vc is MyProfileViewController ||
                vc is VacanciesViewController ||
                vc is ContactViewController  {
                return true
            }
        }
        return false
    }
    
}
