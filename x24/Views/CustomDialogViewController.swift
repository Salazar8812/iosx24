//
//  CustomDialogViewController.swift
//  x24
//
//  Created by Charls Salazar on 23/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

protocol CustomDialogDelegate {
    func OnClickCancel()
    func OnClickOk()
}

class CustomDialogViewController: UIViewController {
    @IBOutlet weak var mTitleDialogLabel: UILabel!
    @IBOutlet weak var mNumberDialogTexfield: IconTextField!
    @IBOutlet weak var mCompanyDialogTextField: IconTextField!
    var mCustomDialogDelegate : CustomDialogDelegate!
    
    @IBOutlet weak var mContentDialogView: UIView!
    @IBOutlet weak var mOkDialogButton: UIButton!
    @IBOutlet weak var mCancelDialogButton: UIButton!
    
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
        animateView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
        
        //self.mCancelDialogButton.layer.borderWidth = 1
        //self.mCancelDialogButton.layer.borderColor = UIColor(netHex: Colors.color_grey) as! CGColor
        
        //self.mOkDialogButton.layer.borderWidth = 1
        //self.mOkDialogButton.layer.borderColor = UIColor(netHex: Colors.color_grey) as! CGColor
        
    }
    
    func setupView() {
        mContentDialogView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func animateView() {
        mContentDialogView.alpha = 0;
        self.mContentDialogView.frame.origin.y = self.mContentDialogView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.mContentDialogView.alpha = 1.0;
            self.mContentDialogView.frame.origin.y = self.mContentDialogView.frame.origin.y - 50
        })
    }
    
    @IBAction func mCancelDialogButton(_ sender: Any) {
        mNumberDialogTexfield.resignFirstResponder()
        mCustomDialogDelegate?.OnClickCancel()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func mOkDialogButton(_ sender: Any) {
        mNumberDialogTexfield.resignFirstResponder()
        mCustomDialogDelegate?.OnClickOk()
        self.dismiss(animated: true, completion: nil)
    }
}
