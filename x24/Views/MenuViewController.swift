//
//  MenuViewController.swift
//  x24
//
//  Created by Charls Salazar on 29/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController,TableViewCellClickDelegate {
    
    var viewController: UIViewController!
    @IBOutlet weak var mMenuListTableView: UITableView!
    var baseDataSourceMenu : BaseDataSource<NSObject, OptionMenuTableViewCell>?
    var listOptionsMenu : [Menu] = []
    var itemMenuSelected : Menu?
    var login : UIViewController!
    var promotions : UIViewController!
    var closeToYou : UIViewController!
    var myQR : UIViewController!
    var profile : UIViewController!
    var contact : UIViewController!
    var vacancies : UIViewController!
    
    
    @IBOutlet var viewCloseSesion: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        baseDataSourceMenu = BaseDataSource(tableView: mMenuListTableView,delegate: self)
        
        defineMenu()
        
        getControllers()
    }

    func getControllers(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let login = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.login = UINavigationController(rootViewController: login)
        
        let promotions = storyboard.instantiateViewController(withIdentifier: "PromotionsViewController") as! PromotionsViewController
        self.promotions = UINavigationController(rootViewController: promotions)
        
        let closeToYou = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.closeToYou = UINavigationController(rootViewController: closeToYou)
        
        let myQR = storyboard.instantiateViewController(withIdentifier: "MyQRViewController") as! MyQRViewController
        self.myQR = UINavigationController(rootViewController: myQR)
        
        let myProfile = storyboard.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
        self.profile = UINavigationController(rootViewController: myProfile)
        
        let contact = storyboard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        self.contact = UINavigationController(rootViewController: contact)
        
        let vacancies = storyboard.instantiateViewController(withIdentifier: "VacanciesViewController") as! VacanciesViewController
        self.vacancies = UINavigationController(rootViewController: vacancies)
        
        self.promotions = UINavigationController(rootViewController: promotions)
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func populateMenuUserLogin(){
        
        let option0 = Menu()
        option0.tileMenu = "CeldaPrimerDato"
        listOptionsMenu.append(option0)
        
        let option1 = Menu()
        option1.tileMenu = "Promociones"
        option1.iconImageMenu = "ic_promotions"
        listOptionsMenu.append(option1)
        
        let option2 = Menu()
        option2.tileMenu = "Cerca de ti"
        option2.iconImageMenu = "ic_close_to_you"
        listOptionsMenu.append(option2)
        
        let option3 = Menu()
        option3.tileMenu = "Mi QR"
        option3.iconImageMenu = "ic_my_qr"
        listOptionsMenu.append(option3)
        
        let option4 = Menu()
        option4.tileMenu = "Mi Perfil"
        option4.iconImageMenu = "ic_my_profile"
        listOptionsMenu.append(option4)
        
        let option6 = Menu()
        option6.tileMenu = "Vacantes"
        option6.iconImageMenu = "ic_vacancy"
        listOptionsMenu.append(option6)
        
        let option7 = Menu()
        option7.tileMenu = "Datos de Facturación"
        option7.iconImageMenu = "ic_billling"
        listOptionsMenu.append(option7)
        
        let option5 = Menu()
        option5.tileMenu = "Contacto"
        option5.iconImageMenu = "ic_contact"
        listOptionsMenu.append(option5)
    }
    
    func populateMenuUserNotLogin(){
        
        let option0 = Menu()
        option0.tileMenu = "CeldaPrimerDato"
        listOptionsMenu.append(option0)
        
        let option2 = Menu()
        option2.tileMenu = "Cerca de ti"
        option2.iconImageMenu = "ic_close_to_you"
        listOptionsMenu.append(option2)
        
        let option1 = Menu()
        option1.tileMenu = "Promociones"
        option1.iconImageMenu = "ic_promotions"
        listOptionsMenu.append(option1)
        
  
        let option5 = Menu()
        option5.tileMenu = "Contacto"
        option5.iconImageMenu = "ic_contact"
        listOptionsMenu.append(option5)
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        itemMenuSelected = item as? Menu
        switch itemMenuSelected?.tileMenu {
        case "CeldaPrimerDato"?:
           self.slideMenuController()?.changeMainViewController(login, close: true)
            break
        case "Promociones"?:
            self.slideMenuController()?.changeMainViewController(promotions, close: true)
            break
        case "Cerca de ti"?:
            self.slideMenuController()?.changeMainViewController(closeToYou, close: true)
            break
        case "Mi QR"?:
            self.slideMenuController()?.changeMainViewController(myQR, close: true)
            break
        case "Mi Perfil"?:
            self.slideMenuController()?.changeMainViewController(profile, close: true)
            break
        case "Contacto"?:
            self.slideMenuController()?.changeMainViewController(contact, close: true)
            break
        case "Vacantes"?:
            self.slideMenuController()?.changeMainViewController(vacancies, close: true)
            break
        default:
            break
        }
    }
    
    @IBAction func mCloseSessionButton(_ sender: Any) {
        //1. Create the alert controller.
        let alert = UIAlertController(title: "x24", message: "Esta Seguro que desea salir", preferredStyle: .alert)
    
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { [weak alert] (_) in
        }))
        
        alert.addAction(UIAlertAction(title: "Salir", style: .default, handler: { [weak alert] (_) in
            
            UserDefaults.standard.setValue("false", forKey: DataPersistent.TERMINAL_LOGIN)
            UserDefaults.standard.setValue(" ", forKey: DataPersistent.TERMINAL_TOKEN)
            UserDefaults.standard.synchronize()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            let mainViewController = UINavigationController(rootViewController: mainController)
            self.slideMenuController()?.changeMainViewController(mainViewController, close: true)
            
            self.defineMenu()
            
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    public func defineMenu()
    {
        listOptionsMenu.removeAll()
        let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
        print("TERMINAL_LOGIN " + result )
        
        if (result) == "true"
        {
            print("populateMenuUserLogin")
            viewCloseSesion.isHidden = false
            populateMenuUserLogin()
        }
        else
        {
            print("populateMenuUserNotLogin")
            viewCloseSesion.isHidden = true
            populateMenuUserNotLogin()
        }
        
        baseDataSourceMenu?.update(items: listOptionsMenu)
        
        UIView.animate(withDuration: 0.3, delay: 1.0, options: [], animations: {
            self.mMenuListTableView.setContentOffset(CGPoint.zero, animated: true)
            
            let numberOfSections = self.mMenuListTableView.numberOfSections
            let numberOfRows = self.mMenuListTableView.numberOfRows(inSection: numberOfSections-1)
            
            let indexPath = IndexPath(row: numberOfRows-1 , section: numberOfSections-1)
            self.mMenuListTableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.middle, animated: true)
            
        }) { (finished) in
            //Perform segue
        }
        
    }
}
