//
//  SearchStoreViewController.swift
//  x24
//
//  Created by Charls Salazar on 09/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class SearchStoreViewController: PullUpController {
    @IBOutlet weak var mListResultTableView: UITableView!
    @IBOutlet weak var mSecondPreviewView: UIView!
    @IBOutlet weak var mFirstPreviewView: UIView!
    @IBOutlet weak var mSearchSeparatorView: UIView!{
        didSet {
            mSearchSeparatorView.layer.cornerRadius = mSearchSeparatorView.frame.height/2
        }
    }
    @IBOutlet weak var mSearchBoxContainerView: UIView!
    @IBOutlet weak var mVisualEffectView: UIVisualEffectView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.willMoveToStickyPoint = { point in
            print("willMoveToStickyPoint \(point)")
        }
        
        self.didMoveToStickyPoint = { point in
            print("didMoveToStickyPoint \(point)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.layer.cornerRadius = 12
    }
    
    override var pullUpControllerPreferredSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: mSecondPreviewView.frame.maxY)
    }
    
    override var pullUpControllerPreviewOffset: CGFloat {
        return mSearchBoxContainerView.frame.height
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        return [mFirstPreviewView.frame.maxY]
    }
    
    override var pullUpControllerIsBouncingEnabled: Bool {
        return false
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return CGRect(x: 5, y: 5, width: 280, height: UIScreen.main.bounds.height - 10)
    }
    
}

extension SearchStoreViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let lastStickyPoint = pullUpControllerAllStickyPoints.last {
            pullUpControllerMoveToVisiblePoint(lastStickyPoint, completion: nil)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}

