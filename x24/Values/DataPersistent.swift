//
//  DataPersistent.swift
//  x24
//
//  Created by Josué :D on 27/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation

class DataPersistent: NSObject {
    
    static let TERMINAL_INITIALIZE = "TERMINAL_INITIALIZE"
    
    static let TERMINAL_LOGIN = "TERMINAL_LOGIN"
    static let TERMINAL_TOKEN = "TERMINAL_TOKEN"
    static let TERMINAL_THUMBNAIL = "TERMINAL_THUMBNAIL"
    static let TERMINAL_USERNAME = "TERMINAL_USERNAME"
    static let TERMINAL_EMAIL = "TERMINAL_EMAIL"
    static let TERMINAL_ID = "TERMINAL_ID"
    static let TERMINAL_FIRST_NAME = "TERMINAL_FIRST_NAME"
    static let TERMINAL_LAST_NAME = "TERMINAL_LAST_NAME"
    
    public func initializeDataPersistent()
    {
        let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_INITIALIZE))
        print(result)
        if result == "nil"
        {
            
            UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_INITIALIZE)
            
            UserDefaults.standard.setValue("", forKey: DataPersistent.TERMINAL_LOGIN)
            UserDefaults.standard.setValue("", forKey: DataPersistent.TERMINAL_TOKEN)
            UserDefaults.standard.setValue("", forKey: DataPersistent.TERMINAL_THUMBNAIL)
            UserDefaults.standard.setValue("", forKey: DataPersistent.TERMINAL_USERNAME)
            UserDefaults.standard.setValue("", forKey: DataPersistent.TERMINAL_EMAIL)
            UserDefaults.standard.setValue("", forKey: DataPersistent.TERMINAL_ID)
            UserDefaults.standard.setValue("", forKey: DataPersistent.TERMINAL_FIRST_NAME)
            UserDefaults.standard.setValue("", forKey: DataPersistent.TERMINAL_LAST_NAME)
            
        }
    }
}



//"token": "398e92b9bf4903abd27f3c6d0bbca82d5b26b64e",
//"username": "test",
//"email": "test@test.com",
//"id": "eea4d53d-1003-4337-b2d7-5a51f4a17259",
//"thumbnail": "/media/CACHE/images/users/test/3a764b160b52ed5cb8a829dc64ac1bd8.jpg",
//"first_name": "test",
//"last_name": "test test"

