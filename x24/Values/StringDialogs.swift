//
//  StringDialogs.swift
//  x24
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class StringDialogs: NSObject {
    static let dialog_error_connection = "Revise su conexión a internet"
    static let dialog_error_intern = "Ha ocurrido un error. Intente nuevamente"
    static let dialog_error_empty = "Este campo es obligatorio"
    static let dialog_error_expiration_date = "La fecha de expiración no es válida"
    
    static let dialog_error_empty_fields = "No deje campos vacíos"
    static let dialog_error_empty_fields_new = "Valide todos los campos obligatorios"

    static let dialog_success_save_new_card = "Tarjeta registrada con éxito"
}
