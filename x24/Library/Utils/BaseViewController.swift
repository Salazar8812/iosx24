//
//  BaseViewController.swift
//  x24
//
//  Created by Charls Salazar on 01/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, UITextFieldDelegate {
    
    var mPresenter : BasePresenter?
    var mPresenters : [BasePresenter]! = []
    var extras : [String: AnyObject] = [:]
    var resultDelegate : ControllerResultDelegate?
    var requestValue : String = ""
    var resultValue : ViewControllerResult = ViewControllerResult.RESULT_ERROR
    var data : [String : AnyObject] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        mPresenter = getPresenter()
        mPresenters = getPresenters()
        if mPresenter != nil {
            mPresenter?.viewDidLoad()
        }
        for presenter in mPresenters! {
            presenter.viewDidLoad()
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if mPresenter != nil {
            mPresenter?.viewDidAppear()
        }
        for presenter in mPresenters! {
            presenter.viewDidAppear()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if resultDelegate != nil {
            resultDelegate?.viewControllerForResult(keyRequest: requestValue, result: resultValue, data: data)
        }
        if mPresenter != nil {
            mPresenter?.viewDidDisappear()
        }
        for presenter in mPresenters! {
            presenter.viewDidDisappear()
        }
    }
    
    func getPresenter() -> BasePresenter? {
        return nil
    }
    
    func getPresenters() -> [BasePresenter]? {
        return []
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if mPresenter != nil {
            mPresenter?.viewWillAppear()
        }
        for presenter in mPresenters! {
            presenter.viewWillAppear()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if mPresenter != nil {
            mPresenter?.viewWillDisappear()
        }
        for presenter in mPresenters! {
            presenter.viewWillDisappear()
        }
    }
    
    func onViewControllerResult() {
        
    }
    
    func onViewControllerResult(params: [String : String]?) {
        
    }
    
    func hasExtra(key: KeysEnum) -> Bool{
        return self.extras[key] != nil
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    func resignFirstResponser(textFields : UITextField...){
        for textField in textFields{
            textField.resignFirstResponder()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
}


