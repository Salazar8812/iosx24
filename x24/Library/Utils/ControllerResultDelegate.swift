//
//  ControllerResultDelegate.swift
//  x24
//
//  Created by Charls Salazar on 01/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

public enum ViewControllerResult{
    case RESULT_OK
    case RESULT_ERROR
}

public protocol ControllerResultDelegate: NSObjectProtocol {
    
    func viewControllerForResult(keyRequest : String, result : ViewControllerResult, data : [String : AnyObject])
    
}

