//
//  BasePresenter.swift
//  x24
//
//  Created by Charls Salazar on 01/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

class BasePresenter: NSObject {
    
    var mViewController : BaseViewController
    
    init(viewController : BaseViewController){
        self.mViewController = viewController
    }
    
    func viewDidLoad(){
    }
    
    func viewWillAppear(){
    }
    
    func viewDidAppear(){
    }
    
    func viewWillDisappear(){
    }
    
    func viewDidDisappear(){
    }
    
    func viewDidUnload(){
    }
    
}

