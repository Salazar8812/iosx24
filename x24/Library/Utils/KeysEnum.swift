//
//  KeysEnum.swift
//  x24
//
//  Created by Charls Salazar on 01/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//
import UIKit

enum KeysEnum: APIKeys {
    
    case EXTRA_WORK_ORDER
    case EXTRA_TIME_TO_ARRIVE_RESPONSE
    case EXTRA_ARR_FAMILY
    case EXTRA_DIRECCION_BEAN
    case EXTRA_PLAN
    case EXTRA_CAPTURE_IMAGE
    case EXTRA_TYPE_PLAN_SELECTED
}

