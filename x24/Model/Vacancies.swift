//
//  Vacancies.swift
//  x24
//
//  Created by Josué :D on 18/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit

class Vacancies: NSObject, Mappable{
    var id: String?
    var title: String?
    var image: String?
    var description2: String?
    var date_start: String?
    var date_finish: String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal func mapping(map: Map) {
        self.id <- map["id"]
        self.title <- map["title"]
        self.image <- map["image"]
        self.description2 <- map["description"]
        self.date_start <- map["date_start"]
        self.date_finish <- map["date_finish"]
    }
}
