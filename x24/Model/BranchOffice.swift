//
//  BranchOffice.swift
//  x24
//
//  Created by Charls Salazar on 21/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation
import ObjectMapper

class BranchOffice : NSObject , Mappable{
    var id : String?
    var name : String?
    var full_address : String?
    var phone : String?
    var accept_order : Bool?
    var city_model : CityModel?
    var latitude : Double?
    var longitude : Double?
    
    
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
        self.full_address <- map["full_address"]
        self.phone <- map["phone"]
        self.accept_order <- map["accept_order"]
        self.city_model <- map["city_model"]
        self.latitude <- map["latitude"]
        self.longitude <- map["longitude"]
    }
}
