//
//  CompanyModel.swift
//  x24
//
//  Created by Charls Salazar on 22/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import ObjectMapper

class CompanyModel: NSObject, Mappable{
    var id : String?
    var name : String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
    }
}
