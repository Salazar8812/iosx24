//
//  CityModel.swift
//  x24
//
//  Created by Charls Salazar on 21/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit

class CityModel : NSObject, Mappable{
    var id : String?
    var name : String?
   
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
    }
}
