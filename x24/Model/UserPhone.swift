//
//  UserPhone.swift
//  x24
//
//  Created by Charls Salazar on 15/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit



class UserPhone: NSObject, Mappable{
    var id : String?
    var phone : String?
    var companyModel : CompanyModel?
    var user : String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal func mapping(map: Map) {
        self.phone <- map["phone"]
        self.companyModel <- map["company_model"]
        self.user <- map["name"]
        self.id <- map["id"]
    }
}
