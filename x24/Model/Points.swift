//
//  Points.swift
//  x24
//
//  Created by Josué :D on 28/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import ObjectMapper

class Points: NSObject, Mappable{
    var points : String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    internal func mapping(map: Map) {
        self.points <- map["points"]
    }
}

